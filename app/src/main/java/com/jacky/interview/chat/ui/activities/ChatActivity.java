package com.jacky.interview.chat.ui.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.jacky.interview.chat.FirebaseChatMainApp;
import com.jacky.interview.chat.R;
import com.jacky.interview.chat.core.chat.ChatPresenter;
import com.jacky.interview.chat.models.Chat;
import com.jacky.interview.chat.ui.fragments.ChatFragment;
import com.jacky.interview.chat.utils.Constants;

import java.util.ArrayList;
import java.util.List;


public class ChatActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private ChatPresenter mChatPresenter;
    private ChatFragment currentFragment;
    private List<Chat> history;

    public static void startActivity(Context context,
                                     String receiver,
                                     String receiverUid,
                                     String firebaseToken) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(Constants.ARG_RECEIVER, receiver);
        intent.putExtra(Constants.ARG_RECEIVER_UID, receiverUid);
        intent.putExtra(Constants.ARG_FIREBASE_TOKEN, firebaseToken);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        bindViews();
        init();
    }

    private void bindViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_listing, menu);
        menu.findItem(R.id.action_logout).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_export:
                exportData();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void exportData() {
        currentFragment.exportHistory();
    }


    private void init() {
        // set the toolbar
        setSupportActionBar(mToolbar);
        // set toolbar title
        mToolbar.setTitle(getIntent().getExtras().getString(Constants.ARG_RECEIVER));
        currentFragment = new ChatFragment();
        currentFragment = ChatFragment.newInstance(getIntent().getExtras().getString(Constants.ARG_RECEIVER),
                getIntent().getExtras().getString(Constants.ARG_RECEIVER_UID),
                getIntent().getExtras().getString(Constants.ARG_FIREBASE_TOKEN));
        // set the register screen fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_content_chat,currentFragment,
                ChatFragment.class.getSimpleName());
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseChatMainApp.setChatActivityOpen(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseChatMainApp.setChatActivityOpen(false);
    }
}
